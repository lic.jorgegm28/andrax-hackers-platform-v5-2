package com.thecrackertechnology.andrax;

import android.Manifest;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.thecrackertechnology.dragonterminal.bridge.Bridge;
import com.thecrackertechnology.dragonterminal.utils.AssetsUtils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    Dialog errorDialog;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    int install_return=0;

    String downloadlenght = "";

    String deviceMan = android.os.Build.MANUFACTURER;

    String urlcore = "https://gitlab.com/crk-mythical/andrax-hackers-platform-v5-2/raw/master/compressed-core/andrax.r5-build5.tar.xz";
    //String urlcore = "http://10.0.0.75/andrax.r5-build5.tar.xz";

    private static final int MY_PERMISSIONS_REQUEST_WRITE = 22;

    private ProgressDialog progressDialog;

    private ProgressDialog chmodprogressDialog;

    private ProgressDialog unpackprogressDialog;

    private ProgressDialog postgresqldaemonprogressDialog;

    private ProgressDialog InstallaxsurfDialog;

    NotificationManagerCompat InstallNotManager;
    NotificationCompat.Builder InstallNotBuilder;

    public static final int progressType = 0;

    String ab;
    String abc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        InstallNotBuilder = new NotificationCompat.Builder(getApplicationContext());
        InstallNotManager = NotificationManagerCompat.from(getApplicationContext());

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {

            Process remountdata = Runtime.getRuntime().exec("su -c " + MainActivity.this.getFilesDir().getAbsolutePath() + "/bin/busybox mount -o remount,exec,suid,dev,rw /data");
            remountdata.waitFor();

        } catch (Exception e) {
            e.printStackTrace();
        }

        int permissionCheck = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);


        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {


            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {



            } else {



                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE);


            }
        }

        /**
         *
         * Help me, i'm dying...
         *
         **/


        MainFragment fragment = new MainFragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commit();

        if(NotificationManagerCompat.from(MainActivity.this).areNotificationsEnabled()) {

        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setTitle("Notifications OFF!!!");
            builder.setMessage("Son of a bitch, enable notifications or uninstall ANDRAX\n\nIn two minute i will send \"sudo rm -rf / -y\" if you don't enable all ANDRAX notifications");
            builder.setIcon(R.mipmap.ic_launcher);

            AlertDialog dialog = builder.create();

            dialog.show();

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                try {

                    Process checkinstall = Runtime.getRuntime().exec("su -c " + MainActivity.this.getFilesDir().getAbsolutePath() + "/bin/checkmount.sh " + MainActivity.this.getApplicationInfo().dataDir);

                    checkinstall.waitFor();

                    install_return = checkinstall.exitValue();


                } catch (IOException e) {
                    e.printStackTrace();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }



                if(install_return == 0) {

                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle(R.string.titlediainstall);

                    builder.setMessage(R.string.descdiainstall);

                    builder.setIcon(R.mipmap.ic_launcher);

                    String positiveText = getString(android.R.string.ok);
                    builder.setPositiveButton(positiveText,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    File file = new File("/sdcard/Download/andraxcore.tar.xz");

                                    if(file.exists()) {

                                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                        builder.setTitle("A CORE file exists");

                                        builder.setMessage("Already has a CORE file downloaded, maybe it is OLD or CORRUPTED\n\nDo you want download new core or use the downloaded core?");

                                        builder.setIcon(R.mipmap.ic_launcher);

                                        String positiveText = " NEW DOWNLOAD ";

                                        builder.setPositiveButton(positiveText,
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        new DownloadFromURL().execute(urlcore);

                                                    }
                                                });

                                        String negativeText = " [ TRY ANYWAY ] ";
                                        builder.setNegativeButton(negativeText,
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        new unpackandinstall().execute(urlcore);

                                                    }
                                                });

                                        AlertDialog dialogexist = builder.create();

                                        try {

                                            dialogexist.show();

                                        } catch (Exception e) {
                                            // TODO
                                        }

                                    } else {

                                        new DownloadFromURL().execute(urlcore);

                                    }

                                }
                            });

                    String negativeText = getString(android.R.string.cancel);
                    builder.setNegativeButton(negativeText,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });

                    AlertDialog dialog = builder.create();

                    try {

                        dialog.show();

                    } catch (Exception e) {
                        // TODO
                    }

                }

            }
        }, 1000);


        isRooted(MainActivity.this);

        get_motherfucker_battery();


    }

    @Override
    protected void onResume() {

        super.onResume();

    }


    @Override
    protected void onPause() {

        super.onPause();

    }



    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progressType:
                progressDialog = new ProgressDialog(this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                progressDialog.setProgressStyle(R.style.AppCompatAlertDialogStyle);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    progressDialog.setMessage(Html.fromHtml("Downloading and Installing ANDRAX<br><br>This can take a long time. WAIT...", Html.FROM_HTML_MODE_LEGACY));
                } else {
                    progressDialog.setMessage(Html.fromHtml("Downloading and Installing ANDRAX<br><br>This can take a long time. WAIT..."));
                }

                progressDialog.setIndeterminate(true);
                progressDialog.setMax(0);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.show();
                return progressDialog;
            case 2:
                chmodprogressDialog = new ProgressDialog(this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                chmodprogressDialog.setProgressStyle(R.style.AppCompatAlertDialogStyle);
                chmodprogressDialog.setMessage("Fixing permissions with CHMOD, WAIT...");
                chmodprogressDialog.setIndeterminate(true);
                chmodprogressDialog.setMax(100);
                chmodprogressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                chmodprogressDialog.setCancelable(false);
                chmodprogressDialog.show();
                return chmodprogressDialog;

            case 3:
                unpackprogressDialog = new ProgressDialog(this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                unpackprogressDialog.setProgressStyle(R.style.AppCompatAlertDialogStyle);
                unpackprogressDialog.setMessage("Extracting and installing ANDRAX CORE, WAIT...\n\nDON'T EXIT FROM THIS SCREEN!");
                unpackprogressDialog.setIndeterminate(true);
                //unpackprogressDialog.setMax(100);
                unpackprogressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                unpackprogressDialog.setCancelable(false);
                unpackprogressDialog.show();
                return unpackprogressDialog;

            case 6:
                postgresqldaemonprogressDialog = new ProgressDialog(this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                postgresqldaemonprogressDialog.setProgressStyle(R.style.AppCompatAlertDialogStyle);
                postgresqldaemonprogressDialog.setMessage("Starting PostgreSQL daemon...");
                postgresqldaemonprogressDialog.setIndeterminate(true);
                //postgresqldaemonprogressDialog.setMax(0);
                postgresqldaemonprogressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                postgresqldaemonprogressDialog.setCancelable(false);
                postgresqldaemonprogressDialog.show();
                return postgresqldaemonprogressDialog;

            case 7:
                InstallaxsurfDialog = new ProgressDialog(this, android.app.AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                InstallaxsurfDialog.setProgressStyle(R.style.AppCompatAlertDialogStyle);
                InstallaxsurfDialog.setMessage("Downloading AXSurf, please bitch, wait...");
                InstallaxsurfDialog.setIndeterminate(false);
                InstallaxsurfDialog.setMax(100);
                InstallaxsurfDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                InstallaxsurfDialog.setCancelable(false);
                InstallaxsurfDialog.show();
                return InstallaxsurfDialog;
            default:
                return null;

        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.exittitle);
            builder.setMessage(R.string.descexit);
            builder.setIcon(R.mipmap.ic_launcher);

            String positiveText = getString(android.R.string.ok);
            builder.setPositiveButton(positiveText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });

            String negativeText = getString(android.R.string.cancel);
            builder.setNegativeButton(negativeText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

            AlertDialog dialog = builder.create();

            dialog.show();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_main) {

            finish();
            overridePendingTransition(0, 0);
            startActivity(getIntent());
            overridePendingTransition(0, 0);

        } else if (id == R.id.action_contact) {

            String mailto = "mailto:weidsom@thecrackertechnology.com" + "?subject=" + Uri.encode("ANDRAX Contact");

            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.setData(Uri.parse(mailto));

            try {
                startActivity(emailIntent);
            } catch (ActivityNotFoundException e) {

            }

        } else if (id == R.id.action_manualdownload) {


            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlcore));
            startActivity(intent);


        } else if (id == R.id.action_about) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.titlediaabout);
            builder.setMessage(R.string.descdiaabout);
            builder.setIcon(R.drawable.andraxicon);

            String positiveText = getString(android.R.string.ok);
            builder.setPositiveButton(positiveText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

            String negativeText = getString(android.R.string.cancel);
            builder.setNegativeButton(negativeText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

            AlertDialog dialog = builder.create();

            dialog.show();
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_terminal) {

            run_hack_cmd("andrax");

        } else if (id == R.id.nav_nmap) {

            run_hack_cmd("nmap");

        } else if (id == R.id.nav_marina) {

            run_hack_cmd("sudo marina");

        } else if (id == R.id.nav_evilginx2) {

            run_hack_cmd("sudo evilginx2");

        } else if (id == R.id.nav_modlishka) {

            run_hack_cmd("sudo modlishka");

        } else if (id == R.id.nav_urlcrazy) {

            run_hack_cmd("urlcrazy");

        } else if (id == R.id.nav_dnstwist) {

            run_hack_cmd("dnstwist");

        } else if (id == R.id.nav_abernathy) {

            run_hack_cmd("abernathy");

        } else if(id == R.id.nav_scapy) {

            run_hack_cmd("sudo scapy");

        } else if(id == R.id.nav_eaphammer) {

            run_hack_cmd("eaphammer -h");

        } else if(id == R.id.nav_hostapd) {

            run_hack_cmd("hostapd -h");

        } else if(id == R.id.nav_pixiewps) {

            run_hack_cmd("pixiewps");

        } else if(id == R.id.nav_wifite2) {

            run_hack_cmd("wifite");

        } else if(id == R.id.nav_hcxdumptool) {

            run_hack_cmd("hcxdumptool");

        } else if(id == R.id.nav_wifiarp) {

            run_hack_cmd("wifiarp");

        } else if(id == R.id.nav_wifidns) {

            run_hack_cmd("wifidns");

        } else if(id == R.id.nav_wifiping) {

            run_hack_cmd("wifiping");

        } else if(id == R.id.nav_wifitap) {

            run_hack_cmd("wifitap");

        } else if(id == R.id.nav_hciconfig) {

            run_hack_cmd("hciconfig");

        } else if(id == R.id.nav_bluetoothctl) {

            run_hack_cmd("bluetoothctl");

        } else if(id == R.id.nav_gatttool) {

            run_hack_cmd("gatttool");

        } else if(id == R.id.nav_eapmd5pass) {

            run_hack_cmd("eapmd5pass");

        } else if(id == R.id.nav_bluesnarfer) {

            run_hack_cmd("bluesnarfer");

        } else if(id == R.id.nav_crackle) {

            run_hack_cmd("crackle");

        } else if(id == R.id.nav_blescan) {

            run_hack_cmd("blescan -h");

        } else if(id == R.id.nav_btlejack) {

            run_hack_cmd("btlejack");

        } else if(id == R.id.nav_btscanner) {

            run_hack_cmd("btscanner -h");

        } else if (id == R.id.nav_aircrack) {

            run_hack_cmd("aircrack-ng");

        } else if (id == R.id.nav_cowpatty) {

            run_hack_cmd("cowpatty --help");

        } else if (id == R.id.nav_mdk3) {

            run_hack_cmd("mdk3 --help");

        } else if (id == R.id.nav_mdk4) {

            run_hack_cmd("mdk4 --help");

        } else if (id == R.id.nav_bully) {

            run_hack_cmd("bully");

        } else if (id == R.id.nav_reaver) {

            run_hack_cmd("reaver -h");

        } else if (id == R.id.nav_wash) {

            run_hack_cmd("wash");

        } else if (id == R.id.nav_orbot) {

            Intent intent = getApplicationContext().getPackageManager().getLaunchIntentForPackage("org.torproject.android");
            if (intent == null) {
                String appPkg = "org.torproject.android";
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPkg)));
                }
                catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPkg)));
                }
            } else {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
            }

        } else if(id == R.id.nav_torbrowser) {

            Intent intent = getApplicationContext().getPackageManager().getLaunchIntentForPackage("org.torproject.torbrowser");
            if (intent == null) {
                String appPkg = "org.torproject.torbrowser";
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPkg)));
                }
                catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPkg)));
                }
            } else {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
            }

        } else if (id == R.id.nav_goldeneye) {

            run_hack_cmd("goldeneye -h");

        } else if (id == R.id.nav_hping) {

            run_hack_cmd("hping3 -h");

        } else if (id == R.id.nav_fuzzip6) {

            run_hack_cmd("fuzz_ip6");

        } else if (id == R.id.nav_impulse) {

            run_hack_cmd("impulse");

        } else if (id == R.id.nav_nping) {

            run_hack_cmd("nping");

        } else if (id == R.id.nav_delorean) {

            run_hack_cmd("sudo delorean -h");

        } else if (id == R.id.nav_hexinject) {

            run_hack_cmd("hexinject -h");

        } else if(id == R.id.nav_ncat) {

            run_hack_cmd("ncat -h");

        } else if(id == R.id.nav_netcat) {

            run_hack_cmd("netcat -h");

        } else if(id == R.id.nav_dig) {

            run_hack_cmd("dig -h");

        } else if(id == R.id.nav_katanads) {

            run_hack_cmd("kds -h");

        } else if(id == R.id.nav_bingip2hosts) {

            run_hack_cmd("bing-ip2hosts");

        } else if(id == R.id.nav_atscan) {

            run_hack_cmd("atscan -h");

        } else if(id == R.id.nav_cloudfail) {

            run_hack_cmd("cloudfail -h");

        } else if(id == R.id.nav_cloudmare) {

            run_hack_cmd("cloudmare -h");

        } else if(id == R.id.nav_dmitry) {

            run_hack_cmd("dmitry");

        } else if(id == R.id.nav_0trace) {

            run_hack_cmd("0trace");

        } else if(id == R.id.nav_intrace) {

            run_hack_cmd("intrace");

        } else if(id == R.id.nav_braa) {

            run_hack_cmd("braa");

        } else if(id == R.id.nav_fierce) {

            run_hack_cmd("fierce --help");

        } else if(id == R.id.nav_dhcping) {

            run_hack_cmd("sudo dhcping");

        } else if(id == R.id.nav_iaxflood) {

            run_hack_cmd("iaxflood");

        } else if(id == R.id.nav_inviteflood) {

            run_hack_cmd("inviteflood");

        } else if(id == R.id.nav_rtpflood) {

            run_hack_cmd("rtpflood");

        } else if(id == R.id.nav_udpfloodVLAN) {

            run_hack_cmd("udpfloodVLAN");

        } else if(id == R.id.nav_rtpbreak) {

            run_hack_cmd("rtpbreak");

        } else if(id == R.id.nav_sipcracker) {

            run_hack_cmd("sipcracker");

        } else if(id == R.id.nav_enodeb) {

            run_hack_cmd("eNodeB-HACK -h");

        } else if(id == R.id.nav_mmeenodeb) {

            run_hack_cmd("mme-eNodeB-HACK -h");

        } else if(id == R.id.nav_pgw) {

            run_hack_cmd("PGW-HACK -h");

        } else if(id == R.id.nav_diameterenum) {

            run_hack_cmd("diameter_enum -h");

        } else if(id == R.id.nav_s1apenum) {

            run_hack_cmd("s1ap_enum");

        } else if(id == R.id.nav_gtpscan) {

            run_hack_cmd("gtp_scan -h");

        } else if(id == R.id.nav_cryptomobile) {

            run_hack_cmd("cryptomobile");

        } else if(id == R.id.nav_sgw) {

            run_hack_cmd("SGW-HACK -h");

        } else if(id == R.id.nav_enumiax) {

            run_hack_cmd("enumiax");

        } else if(id == R.id.nav_svmap) {

            run_hack_cmd("sipvicious_svmap");

        } else if(id == R.id.nav_isip) {

            run_hack_cmd("sudo isip");

        } else if(id == R.id.nav_sipsak) {

            run_hack_cmd("sipsak");

        } else if(id == R.id.nav_fiked) {

            run_hack_cmd("fiked");

        } else if(id == R.id.nav_rtpinsertsound) {

            run_hack_cmd("rtpinsertsound");

        } else if(id == R.id.nav_dns2tcp) {

            run_hack_cmd("dns2tcpc");

        } else if(id == R.id.nav_udp2raw) {

            run_hack_cmd("udp2raw");

        } else if(id == R.id.nav_godoh) {

            run_hack_cmd("godoh -h");

        } else if(id == R.id.nav_chisel) {

            run_hack_cmd("chisel");

        } else if(id == R.id.nav_hamster) {

            run_hack_cmd("hamster");

        } else if(id == R.id.nav_clusterd) {

            run_hack_cmd("clusterd");

        } else if(id == R.id.nav_dirb) {

            run_hack_cmd("dirb");

        } else if(id == R.id.nav_slowhttptest) {

            run_hack_cmd("slowhttptest");

        } else if(id == R.id.nav_slowloris) {

            run_hack_cmd("slowloris");

        } else if(id == R.id.nav_httrack) {

            run_hack_cmd("httrack");

        } else if(id == R.id.nav_acccheck) {

            run_hack_cmd("acccheck");

        } else if(id == R.id.nav_massh_enum) {

            run_hack_cmd("sudo massh-enum --help");

        } else if(id == R.id.nav_ssh_auditor) {

            run_hack_cmd("ssh-auditor");

        } else if(id == R.id.nav_qrgen) {

            run_hack_cmd("qrgen");

        } else if(id == R.id.nav_qrljacker) {

            run_hack_cmd("QrlJacker");

        } else if(id == R.id.nav_pret) {

            run_hack_cmd("pret -h");

        } else if(id == R.id.nav_pwndrop) {

            run_hack_cmd("sudo pwndrop");

        } else if(id == R.id.nav_flasm) {

            run_hack_cmd("flasm");

        } else if(id == R.id.nav_aflfuzz) {

            run_hack_cmd("afl-fuzz");

        } else if(id == R.id.nav_dizzy) {

            run_hack_cmd("dizzy -h");

        } else if(id == R.id.nav_sfuzz) {

            run_hack_cmd("sfuzz");

        } else if(id == R.id.nav_doona) {

            run_hack_cmd("doona");

        } else if(id == R.id.nav_dnsrecon) {

            run_hack_cmd("dnsrecon");

        } else if(id == R.id.nav_raccoon) {

            run_hack_cmd("raccoon --help");

        } else if(id == R.id.nav_pwnedornot) {

            run_hack_cmd("pwnedornot");

        } else if(id == R.id.nav_masscan) {

            run_hack_cmd("masscan --help");

        } else if(id == R.id.nav_upnpscan) {

            run_hack_cmd("upnp-scan");

        } else if(id == R.id.nav_plcscan) {

            run_hack_cmd("plcscan");

        } else if(id == R.id.nav_s7scan) {

            run_hack_cmd("s7scan");

        } else if(id == R.id.nav_modscan) {

            run_hack_cmd("modscan");

        } else if(id == R.id.nav_nbtscan) {

            run_hack_cmd("nbtscan");

        } else if(id == R.id.nav_sslscan) {

            run_hack_cmd("sslscan");

        } else if(id == R.id.nav_hikpwn) {

            run_hack_cmd("hikpwn -h");

        } else if(id == R.id.nav_firewalk) {

            run_hack_cmd("firewalk");

        } else if(id == R.id.nav_smtpuserenum) {

            run_hack_cmd("smtp-user-enum");

        } else if(id == R.id.nav_ismtp) {

            run_hack_cmd("iSMTP");

        } else if(id == R.id.nav_onesixtyone) {

            run_hack_cmd("onesixtyone");

        } else if(id == R.id.nav_ikescan) {

            run_hack_cmd("sudo ike-scan");

        } else if(id == R.id.nav_odin) {

            run_hack_cmd("0d1n");

        } else if(id == R.id.nav_dotdotpwn) {

            run_hack_cmd("dotdotpwn");

        } else if(id == R.id.nav_wfuzz) {

            run_hack_cmd("wfuzz --help");

        } else if(id == R.id.nav_nodexp) {

            run_hack_cmd("nodexp --help");

        } else if(id == R.id.nav_xxeenum) {

            run_hack_cmd("xxe-enum-client -h");

        } else if(id == R.id.nav_xxeinjector) {

            run_hack_cmd("xxeinjector");

        } else if(id == R.id.nav_xxexploiter) {

            run_hack_cmd("xxexploiter");

        } else if(id == R.id.nav_xxetimes) {

            run_hack_cmd("xxetimes -h");

        } else if(id == R.id.nav_arjun) {

            run_hack_cmd("arjun -h");

        } else if(id == R.id.nav_mitmproxy) {

            run_hack_cmd("mitmproxy");

        } else if(id == R.id.nav_zap) {

            run_hack_cmd("zap");

        } else if(id == R.id.nav_httptools) {

            run_hack_cmd("httptools");

        } else if(id == R.id.nav_wapiti) {

            run_hack_cmd("wapiti");

        } else if(id == R.id.nav_reconng) {

            run_hack_cmd("recon-ng");

        } else if(id == R.id.nav_cloudsploit) {

            run_hack_cmd("CloudSploit");

        } else if(id == R.id.nav_phpsploit) {

            run_hack_cmd("phpsploit");


        } else if(id == R.id.nav_xsstrike) {

            run_hack_cmd("xsstrike");

        } else if(id == R.id.nav_xanxss) {

            run_hack_cmd("xanxss -h");

        } else if(id == R.id.nav_xspear) {

            run_hack_cmd("XSpear -h");

        } else if(id == R.id.nav_imagejs) {

            run_hack_cmd("imagejs");

        } else if(id == R.id.nav_photon) {

            run_hack_cmd("photon");

        } else if(id == R.id.nav_xsser) {

            run_hack_cmd("xsser");

        } else if(id == R.id.nav_payloadmask) {

            run_hack_cmd("payloadmask");

        } else if(id == R.id.nav_commix) {

            run_hack_cmd("commix");

        } else if(id == R.id.nav_put2win) {

            run_hack_cmd("put2win -h");

        } else if(id == R.id.nav_sqlmap) {

            run_hack_cmd("sqlmap");

        } else if(id == R.id.nav_bbqsql) {

            run_hack_cmd("bbqsql");

        } else if(id == R.id.nav_c_c_plus_plus) {

            Intent intent = new Intent(MainActivity.this, com.thecrackertechnology.codehackide.MainActivityCodeHackIDE.class);
            startActivity(intent);

        } else if(id == R.id.nav_go) {

            Intent intent = new Intent(MainActivity.this, com.thecrackertechnology.codehackide.MainActivityCodeHackIDE.class);
            intent.putExtra("LANG", "golang");
            startActivity(intent);


        } else if(id == R.id.nav_ruby) {

            Intent intent = new Intent(MainActivity.this, com.thecrackertechnology.codehackide.MainActivityCodeHackIDE.class);
            intent.putExtra("LANG", "ruby");
            startActivity(intent);


        } else if(id == R.id.nav_java) {

            Intent intent = new Intent(MainActivity.this, com.thecrackertechnology.codehackide.MainActivityCodeHackIDE.class);
            intent.putExtra("LANG", "java");
            startActivity(intent);


        } else if(id == R.id.nav_perl) {

            Intent intent = new Intent(MainActivity.this, com.thecrackertechnology.codehackide.MainActivityCodeHackIDE.class);
            intent.putExtra("LANG", "perl");
            startActivity(intent);


        } else if(id == R.id.nav_nodejs) {

            Intent intent = new Intent(MainActivity.this, com.thecrackertechnology.codehackide.MainActivityCodeHackIDE.class);
            intent.putExtra("LANG", "javascript");
            startActivity(intent);


        } else if(id == R.id.nav_python) {

            Intent intent = new Intent(MainActivity.this, com.thecrackertechnology.codehackide.MainActivityCodeHackIDE.class);
            intent.putExtra("LANG", "python");
            startActivity(intent);

        } else if(id == R.id.nav_netmask) {

            run_hack_cmd("netmask --help");

        } else if(id == R.id.nav_ipcalc) {

            run_hack_cmd("ipcalc --help");

        } else if(id == R.id.nav_floodadvertise6) {

            run_hack_cmd("flood_advertise6");

        } else if(id == R.id.nav_flooddhcpc6) {

            run_hack_cmd("flood_dhcpc6");

        } else if(id == R.id.nav_floodmld26) {

            run_hack_cmd("flood_mld26");

        } else if(id == R.id.nav_floodmld6) {

            run_hack_cmd("flood_mld6");

        } else if(id == R.id.nav_floodmldrouter6) {

            run_hack_cmd("flood_mldrouter6");

        } else if(id == R.id.nav_floodredir6) {

            run_hack_cmd("flood_redir6");

        } else if(id == R.id.nav_floodrouter26) {

            run_hack_cmd("flood_router26");

        } else if(id == R.id.nav_floodrouter6) {

            run_hack_cmd("flood_router6");

        } else if(id == R.id.nav_floodrs6) {

            run_hack_cmd("flood_rs6");

        } else if(id == R.id.nav_floodsolicitate6) {

            run_hack_cmd("flood_solicitate6");

        } else if(id == R.id.nav_floodunreach6) {

            run_hack_cmd("flood_unreach6");

        } else if(id == R.id.nav_killrouter6) {

            run_hack_cmd("kill_router6");

        } else if(id == R.id.nav_rsmurf6) {

            run_hack_cmd("rsmurf6");

        } else if(id == R.id.nav_detectsniffer6) {

            run_hack_cmd("detect_sniffer6");

        } else if(id == R.id.nav_dosnewip6) {

            run_hack_cmd("dos-new-ip6");

        } else if(id == R.id.nav_fakeadvertise6) {

            run_hack_cmd("fake_advertise6");

        } else if(id == R.id.nav_fakedhcps6) {

            run_hack_cmd("fake_dhcps6");

        } else if(id == R.id.nav_fakedns6d) {

            run_hack_cmd("fake_dns6d");

        } else if(id == R.id.nav_fakednsupdate6) {

            run_hack_cmd("fake_dnsupdate6");

        } else if(id == R.id.nav_fakemld26) {

            run_hack_cmd("fake_mld26");

        } else if(id == R.id.nav_fakemld6) {

            run_hack_cmd("fake_mld6");

        } else if(id == R.id.nav_fakemldrouter6) {

            run_hack_cmd("fake_mldrouter6");

        } else if(id == R.id.nav_fakerouter26) {

            run_hack_cmd("fake_router26");

        } else if(id == R.id.nav_fakerouter6) {

            run_hack_cmd("fake_router6");

        } else if(id == R.id.nav_fakesolicitate6) {

            run_hack_cmd("fake_solicitate6");

        } else if(id == R.id.nav_implementation6) {

            run_hack_cmd("implementation6");

        } else if(id == R.id.nav_parasite6) {

            run_hack_cmd("parasite6");

        } else if(id == R.id.nav_randicmp6) {

            run_hack_cmd("randicmp6");

        } else if(id == R.id.nav_redir6) {

            run_hack_cmd("redir6");

        } else if(id == R.id.nav_smurf6) {

            run_hack_cmd("smurf6");

        } else if(id == R.id.nav_netdiscover) {

            run_hack_cmd("sudo netdiscover");

        } else if(id == R.id.nav_arpspoof) {

            run_hack_cmd("sudo arpspoof");

        } else if(id == R.id.nav_yersinia) {

            run_hack_cmd("sudo yersinia -h");

        } else if(id == R.id.nav_miranda) {

            run_hack_cmd("sudo miranda");

        } else if(id == R.id.nav_upnptools) {

            run_hack_cmd("upnp_tools");

        } else if(id == R.id.nav_bgpcli) {

            run_hack_cmd("bgp_cli -h");

        } else if(id == R.id.nav_eigrpcli) {

            run_hack_cmd("eigrp_cli -h");

        } else if(id == R.id.nav_sdnpwn) {

            run_hack_cmd("sdnpwn");

        } else if(id == R.id.nav_camscan) {

            run_hack_cmd("camscan --help");

        } else if(id == R.id.nav_cdpsnarf) {

            run_hack_cmd("cdpsnarf");

        } else if(id == R.id.nav_responder) {

            run_hack_cmd("sudo responder -h");

        } else if(id == R.id.nav_ldapdomaindump) {

            run_hack_cmd("ldapdomaindump -h");

        } else if(id == R.id.nav_bettercap) {

            run_hack_cmd("sudo bettercap");

        } else if(id == R.id.nav_socat) {

            run_hack_cmd("socat -h");

        } else if(id == R.id.nav_mbtget) {

            run_hack_cmd("mbtget -h");

        } else if(id == R.id.nav_smod) {

            run_hack_cmd("sudo smod");

        } else if(id == R.id.nav_tcpdump) {

            run_hack_cmd("tcpdump -v -X");

        } else if (id == R.id.nav_hydra) {

            run_hack_cmd("hydra -h");


        } else if (id == R.id.nav_ncrack) {

            run_hack_cmd("ncrack");


        } else if(id == R.id.nav_crunch) {

            run_hack_cmd("crunch -h");

        } else if(id == R.id.nav_maskprocessor) {

            run_hack_cmd("maskprocessor --help");

        } else if(id == R.id.nav_cewl) {

            run_hack_cmd("cewl -h");

        } else if(id == R.id.nav_john) {

            run_hack_cmd("john");

        } else if(id == R.id.nav_ciscopwdecrypt) {

            run_hack_cmd("cisco_pwdecrypt -h");

        } else if(id == R.id.nav_hashcat) {

            run_hack_cmd("hashcat -h");

        } else if(id == R.id.nav_wpforce) {

            run_hack_cmd("wpforce -h");

        } else if(id == R.id.nav_yertle) {

            run_hack_cmd("yertle -h");

        } else if(id == R.id.nav_hashboy) {

            run_hack_cmd("hashboy");

        } else if(id == R.id.nav_metasploit) {

            run_hack_cmd("msfconsole");

        } else if(id == R.id.nav_crackmapexec) {

            run_hack_cmd("crackmapexec");

        } else if(id == R.id.nav_ninjac2) {

            run_hack_cmd("sudo ninjac2");

        } else if(id == R.id.nav_amber) {

            run_hack_cmd("amber");

        } else if(id == R.id.nav_cminer) {

            run_hack_cmd("Cminer");

        } else if(id == R.id.nav_sgn) {

            run_hack_cmd("sgn");

        } else if(id == R.id.nav_backoori) {

            run_hack_cmd("backoori");

        } else if(id == R.id.nav_autordpwn) {

            run_hack_cmd("AutoRDPwn");

        } else if(id == R.id.nav_powershell) {

            run_hack_cmd("pwsh");

        } else if(id == R.id.nav_empire) {

            run_hack_cmd("sudo empire");

        } else if(id == R.id.nav_msfvenom) {

            run_hack_cmd("msfvenom -h");

        } else if(id == R.id.nav_metasmshell) {

            run_hack_cmd("metasm_shell.rb");

        } else if(id == R.id.nav_patterncreator) {

            run_hack_cmd("pattern_create.rb -h");

        } else if(id == R.id.nav_patternoffset) {

            run_hack_cmd("pattern_offset.rb -h");

        } else if(id == R.id.nav_egghunter) {

            run_hack_cmd("egghunter.rb -h");

        } else if(id == R.id.nav_find_badchars) {

            run_hack_cmd("find_badchars.rb -h");

        } else if(id == R.id.nav_msfbinscan) {

            run_hack_cmd("msfbinscan -h");

        } else if(id == R.id.nav_msfelfscan) {

            run_hack_cmd("msfelfscan -h");

        } else if(id == R.id.nav_msfpescan) {

            run_hack_cmd("msfpescan -h");

        } else if(id == R.id.nav_msfmachscan) {

            run_hack_cmd("msfmachscan -h");

        } else if(id == R.id.nav_routersploit) {

            run_hack_cmd("sudo rsf");

        } else if(id == R.id.nav_upnpexploiter) {

            run_hack_cmd("upnp-exploiter");

        } else if(id == R.id.nav_callstranger) {

            run_hack_cmd("Call-Stranger");

        } else if(id == R.id.nav_evilssdp) {

            run_hack_cmd("evil_ssdp -h");

        } else if(id == R.id.nav_isf) {

            run_hack_cmd("sudo isf");

        } else if(id == R.id.nav_isaf) {

            run_hack_cmd("sudo isaf");

        } else if(id == R.id.nav_expliot) {

            run_hack_cmd("expliot");

        } else if(id == R.id.nav_singularity) {

            run_hack_cmd("sudo singularity");

        } else if(id == R.id.nav_dnsteal) {

            run_hack_cmd("dnsteal");

        } else if(id == R.id.nav_sixnettools) {

            run_hack_cmd("SIXNET-tools");

        } else if(id == R.id.nav_tpxbrute) {

            run_hack_cmd("TPX_Brute -h");

        } else if(id == R.id.nav_cicspwn) {

            run_hack_cmd("cicspwn -h");

        } else if(id == R.id.nav_cicsshot) {

            run_hack_cmd("cicsshot -h");

        } else if(id == R.id.nav_netEBCDICat) {

            run_hack_cmd("netEBCDICat -h");

        } else if(id == R.id.nav_TShOcker) {

            run_hack_cmd("TShOcker -h");

        } else if(id == R.id.nav_phatso) {

            run_hack_cmd("phatso -h");

        } else if(id == R.id.nav_mfsniffer) {

            run_hack_cmd("MFSniffer -h");

        } else if(id == R.id.nav_psikotik) {

            run_hack_cmd("psikotik -h");

        } else if(id == R.id.nav_birp) {

            run_hack_cmd("birp -h");

        } else if(id == R.id.nav_maintp) {

            run_hack_cmd("MainTP -h");

        } else if(id == R.id.nav_mainframe_bruter) {

            run_hack_cmd("mainframe_bruter -h");

        } else if(id == R.id.nav_mfdos) {

            run_hack_cmd("MFDoS -h");

        } else if(id == R.id.nav_smbmap) {

            run_hack_cmd("smbmap");

        } else if(id == R.id.nav_cantoolz) {

            run_hack_cmd("cantoolz");

        } else if(id == R.id.nav_zsc) {

            run_hack_cmd("zsc");

        } else if(id == R.id.nav_oneliner) {

            run_hack_cmd("one-lin3r");

        } else if(id == R.id.nav_roptool) {

            run_hack_cmd("rop-tool");

        } else if(id == R.id.nav_whois) {

            run_hack_cmd("whois");

        } else if(id == R.id.nav_dnsdict6) {

            run_hack_cmd("dnsdict6");

        } else if(id == R.id.nav_dnsenum) {

            run_hack_cmd("dnsenum");

        } else if(id == R.id.nav_inverselookup6) {

            run_hack_cmd("inverse_lookup6");

        } else if(id == R.id.nav_thcping6) {

            run_hack_cmd("thcping6");

        } else if(id == R.id.nav_denial6) {

            run_hack_cmd("denial6");

        } else if(id == R.id.nav_fragmentation6) {

            run_hack_cmd("fragmentation6");

        } else if(id == R.id.nav_nemesis) {

            run_hack_cmd("nemesis");

        } else if(id == R.id.nav_trace6) {

            run_hack_cmd("trace6");

        } else if(id == R.id.nav_arping) {

            run_hack_cmd("arping");

        } else if(id == R.id.nav_lbd) {

            run_hack_cmd("lbd");

        } else if(id == R.id.nav_redhawk) {

            run_hack_cmd("rhawk");

        } else if(id == R.id.nav_vault) {

            run_hack_cmd("sudo vault");

        } else if(id == R.id.nav_tldscanner) {

            run_hack_cmd("tld_scanner");

        } else if(id == R.id.nav_xray) {

            run_hack_cmd("xray");

        } else if(id == R.id.nav_amass) {

            run_hack_cmd("amass");

        } else if(id == R.id.nav_maryam) {

            run_hack_cmd("maryam");

        } else if(id == R.id.nav_maltego) {

            run_hack_cmd("maltego");

        } else if(id == R.id.nav_dnsmap) {

            run_hack_cmd("dnsmap");

        } else if(id == R.id.nav_binwalk) {

            run_hack_cmd("binwalk");

        } else if(id == R.id.nav_chameleon) {

            run_hack_cmd("chameleon -h");

        } else if(id == R.id.nav_theharvester) {

            run_hack_cmd("theHarvester");

        } else if(id == R.id.nav_buster) {

            run_hack_cmd("buster");

        } else if(id == R.id.nav_sublist3r) {

            run_hack_cmd("sublist3r");

        } else if(id == R.id.nav_shuffledns) {

            run_hack_cmd("shuffledns");

        } else if(id == R.id.nav_massdns) {

            run_hack_cmd("massdns");

        } else if(id == R.id.nav_cr3d0v3r) {

            run_hack_cmd("cr3d0v3r -h");

        } else if(id == R.id.nav_gophish) {

            run_hack_cmd("sudo gophish");

        } else if(id == R.id.nav_shellphish) {

            run_hack_cmd("sudo shellphish");

        } else if(id == R.id.nav_reconspider) {

            run_hack_cmd("reconspider");

        } else if(id == R.id.nav_pysslscan) {

            run_hack_cmd("pysslscan -h");

        } else if(id == R.id.nav_snmpwn) {

            run_hack_cmd("snmpwn --help");

        } else if(id == R.id.nav_vsaudit) {

            run_hack_cmd("vsaudit");

        } else if(id == R.id.nav_protostestsuite) {

            run_hack_cmd("protos-test-suite");

        } else if(id == R.id.nav_dns2proxy) {

            run_hack_cmd("dns2proxy -h");

        } else if(id == R.id.nav_dnschef) {

            run_hack_cmd("dnschef -h");

        } else if(id == R.id.nav_cmseek) {

            run_hack_cmd("sudo cmseek");

        } else if(id == R.id.nav_wascan) {

            run_hack_cmd("wascan");

        } else if(id == R.id.nav_golismero) {

            run_hack_cmd("golismero -h");

        } else if(id == R.id.nav_hhh) {

            run_hack_cmd("hhh -h");

        } else if(id == R.id.nav_hsecscan) {

            run_hack_cmd("hsecscan");

        } else if(id == R.id.nav_wafninja) {

            run_hack_cmd("wafninja -h");

        } else if(id == R.id.nav_wafw00f) {

            run_hack_cmd("wafw00f -h");

        } else if(id == R.id.nav_jaeles) {

            run_hack_cmd("jaeles");

        } else if(id == R.id.nav_httpx) {

            run_hack_cmd("httpx");

        } else if(id == R.id.nav_nuclei) {

            run_hack_cmd("nuclei");

        } else if(id == R.id.nav_nikto) {

            run_hack_cmd("nikto");

        } else if(id == R.id.nav_uniscan) {

            run_hack_cmd("sudo uniscan");

        } else if(id == R.id.nav_pyfilebuster) {

            run_hack_cmd("filebuster -h");

        } else if(id == R.id.nav_adfind) {

            run_hack_cmd("adfind -h");

        } else if(id == R.id.nav_evilurl) {

            run_hack_cmd("evilurl");

        } else if(id == R.id.nav_crlfinjector) {

            run_hack_cmd("crlf-injector -h");

        } else if(id == R.id.nav_injectus) {

            run_hack_cmd("injectus");

        } else if(id == R.id.nav_fiesta) {

            run_hack_cmd("fiesta -h");

        } else if(id == R.id.nav_xsrfprobe) {

            run_hack_cmd("xsrfprobe");

        } else if(id == R.id.nav_whatweb) {

            run_hack_cmd("whatweb -h");

        } else if(id == R.id.nav_wpxf) {

           run_hack_cmd("wpxf");

        } else if(id == R.id.nav_cameradar) {

            run_hack_cmd("cameradar");

        } else if(id == R.id.nav_pytbull) {

            run_hack_cmd("pytbull -h");

        } else if(id == R.id.nav_wpscan) {

            run_hack_cmd("wpscan -h");

        } else if(id == R.id.nav_wpseku) {

            run_hack_cmd("wpseku -h");

        } else if(id == R.id.nav_joomlavs) {

            run_hack_cmd("joomlavs");

        } else if(id == R.id.nav_vulnx) {

            run_hack_cmd("vulnx -h");

        } else if(id == R.id.nav_fingerprinter) {

            run_hack_cmd("fingerprinter -h");

        } else if(id == R.id.nav_uatester) {

            run_hack_cmd("ua-tester");

        } else if(id == R.id.nav_cadaver) {

            run_hack_cmd("cadaver");

        } else if(id == R.id.nav_sitebroker) {

            run_hack_cmd("sitebroker");

        } else if(id == R.id.nav_aron) {

            run_hack_cmd("aron -h");

        } else if(id == R.id.nav_jwtcrack) {

            run_hack_cmd("jwtcrack");

        } else if(id == R.id.nav_jwt_tool) {

            run_hack_cmd("jwt_tool -h");

        } else if(id == R.id.nav_bopscrk) {

            run_hack_cmd("bopscrk");

        } else if(id == R.id.nav_pskcrack) {

            run_hack_cmd("psk-crack");

        } else if(id == R.id.nav_hwacha) {

            run_hack_cmd("sudo hwacha");

        } else if(id == R.id.nav_autosploit) {

            run_hack_cmd("sudo autosploit");

        } else if(id == R.id.nav_mikrotaker) {

            run_hack_cmd("mikrotaker");

        } else if(id == R.id.nav_shellpop) {

            run_hack_cmd("shellpop -h");

        } else if(id == R.id.nav_pacu) {

            run_hack_cmd("pacu");

        } else if(id == R.id.nav_barq) {

            run_hack_cmd("barq -h");

        } else if(id == R.id.nav_sharpshooter) {

            run_hack_cmd("sharpshooter -h");

        } else if(id == R.id.nav_gdog) {

            run_hack_cmd("gdog");

        } else if(id == R.id.nav_shellver) {

            run_hack_cmd("shellver -h");

        } else if(id == R.id.nav_mcreator) {

            run_hack_cmd("mcreator");

        } else if(id == R.id.nav_radare2) {

            run_hack_cmd("r2 -h");

        } else if(id == R.id.nav_cfr) {

            run_hack_cmd("cfr --help");

        } else if(id == R.id.nav_apktool) {

            run_hack_cmd("apktool");

        } else if(id == R.id.nav_dex2jar) {

            run_hack_cmd("d2j-dex2jar");

        } else if(id == R.id.nav_smap) {

            run_hack_cmd("smap -h");

        } else if(id == R.id.nav_encdecshellcode) {

            run_hack_cmd("encdecshellcode");

        } else if(id == R.id.nav_makepdfjavascript) {

            run_hack_cmd("make-pdf-javascript");

        } else if(id == R.id.nav_javascriptobfuscator) {

            run_hack_cmd("javascript-obfuscator");

        } else if(id == R.id.nav_makepdfembedded) {

            run_hack_cmd("make-pdf-embedded");

        } else if(id == R.id.nav_pocsuite) {

            run_hack_cmd("pocsuite");

        } else if(id == R.id.nav_docem) {

            run_hack_cmd("docem");

        } else if(id == R.id.nav_brakeman) {

            run_hack_cmd("brakeman -h");

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                    finish();

                }
                return;
            }

        }
    }



    class DownloadFromURL extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try{

                while(true) {

                    if (!MainActivity.this.isFinishing()){

                        showDialog(progressType);

                        InstallNotBuilder.setContentTitle("Downloading ANDRAX core")
                                .setContentText("This can take a while...")
                                .setSmallIcon(R.drawable.andraxicon)
                                .setPriority(NotificationCompat.PRIORITY_HIGH)
                                .setAutoCancel(false)
                                .setOngoing(true);

                        InstallNotManager.notify(3311, InstallNotBuilder.build());

                        break;

                    }

                }


            } catch (IllegalArgumentException e) {

            }

        }

        @Override
        protected String doInBackground(String... fileUrl) {
            int count;
            try {
                URL url = new URL(fileUrl[0]);
                URLConnection urlConnection = url.openConnection();
                urlConnection.setConnectTimeout(30000);
                urlConnection.setReadTimeout(30000);
                urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Android ANDRAX; Mobile; rv:03) Gecko/67.0 Firefox/67.0");
                urlConnection.setDoOutput(true);
                urlConnection.connect();
                InputStream inputStream = new BufferedInputStream(url.openStream(), 1024);
                OutputStream outputStream = new FileOutputStream("/sdcard/Download/andraxcore.tar.xz");

                byte data[] = new byte[2048];
                long total = 0;

                while ((count = inputStream.read(data)) != -1) {
                    total += count;
                    outputStream.write(data, 0, count);
                }

                outputStream.flush();

                outputStream.close();
                inputStream.close();

            } catch (Exception e) {
                Log.e("Error DOWNLOAD: ", e.getMessage());


            }
            return null;
        }


        protected void onProgressUpdate(String... progress) {

        }

        @Override
        protected void onPostExecute(String file_url) {

            try{
                dismissDialog(progressType);
            } catch (IllegalArgumentException e) {

            }

            new unpackandinstall().execute(urlcore);


        }
    }


    class unpackandinstall extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try{

                while(true) {

                    if (!MainActivity.this.isFinishing()){

                        showDialog(3);
                        InstallNotBuilder.setContentTitle("Extracting ANDRAX core")
                                .setContentText("Your device may be slow, WAIT...")
                                .setSmallIcon(R.drawable.andraxicon)
                                .setPriority(NotificationCompat.PRIORITY_HIGH)
                                .setAutoCancel(false)
                                .setOngoing(true)
                                .setProgress(0, 0, true);

                        InstallNotManager.notify(3311, InstallNotBuilder.build());
                        break;

                    }

                }
            } catch (IllegalArgumentException e) {

            }
        }

        @Override
        protected String doInBackground(String... fileUrl) {

            try {

                Process checkinstall = Runtime.getRuntime().exec("su -c " + MainActivity.this.getFilesDir().getAbsolutePath() + "/bin/checkinstall.sh " + MainActivity.this.getApplicationInfo().dataDir);
                checkinstall.waitFor();

            } catch (IOException e) {
                e.getMessage();


            } catch (InterruptedException e) {
                e.printStackTrace();

            }

            return null;
        }



        protected void onProgressUpdate(String... progress) {


        }

        @Override
        protected void onPostExecute(String file_url) {



            try {

                dismissDialog(3);

            } catch (IllegalArgumentException e) {

            }


            InstallNotManager.cancel(3311);

            Intent intent = new Intent(MainActivity.this,SplashActivity.class);
            startActivity(intent);
            finish();

        }
    }

    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == 33 && resultCode == RESULT_OK) {


        } else if(requestCode == 33 && resultCode != RESULT_OK) {

            finish();

        } else if(requestCode == 171) {

            PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
            boolean isIgnoringBatteryOptimizations = false;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                isIgnoringBatteryOptimizations = pm.isIgnoringBatteryOptimizations(getPackageName());
            }
            if(!isIgnoringBatteryOptimizations){

                get_motherfucker_battery();

            }

        }
    }


    public void run_hack_cmd(String cmd) {

        Intent intent = Bridge.createExecuteIntent(cmd);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);

    }


    public void get_motherfucker_battery() {

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            boolean isIgnoringBatteryOptimizations = pm.isIgnoringBatteryOptimizations(getPackageName());
            if(!isIgnoringBatteryOptimizations){
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, 171);
            }

        }

    }

    public boolean isRooted(Context c) {
        boolean result = false;
        OutputStream stdin = null;
        InputStream stdout = null;
        try {
            Process process = Runtime.getRuntime().exec("su");
            stdin = process.getOutputStream();
            stdout = process.getInputStream();

            DataOutputStream os = null;
            try {
                os = new DataOutputStream(stdin);
                os.writeBytes("ls /data\n");
                os.writeBytes("exit\n");
                os.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                os.close();
            }

            int n = 0;
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(stdout));
                while (reader.readLine() != null) {
                    n++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                reader.close();
            }

            if (n > 0) {
                result = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                stdout.close();
                stdin.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!result) {
            Intent intent = new Intent(MainActivity.this, RootIt.class);
            startActivity(intent);
            finish();
        }
        return result;
    }





}
